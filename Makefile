# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
# Makefile to build and install iPreso plugin
#
# VERSION shall be defined when Makefile is called.
# For example:
# $ make all VERSION=2.3.4

.PHONY: all clean composer player pkg install

ifndef VERSION
VERSION="0.$(shell date +%Y%m%d~%H%M%S)"
endif

MYMAKEFLAGS='VERSION=$(VERSION)'

all: player composer

clean:
	@echo "===================================================="
	@echo "Cleaning target... "
	@$(MAKE) -C player $(MYMAKEFLAGS) --no-print-directory clean
	@$(MAKE) -C composer $(MYMAKEFLAGS) --no-print-directory clean

composer:
	@echo "===================================================="
	@echo "Building composer plugin... "
	@$(MAKE) -C composer $(MYMAKEFLAGS) --no-print-directory all

player:
	@echo "===================================================="
	@echo "Building player plugin... "
	@$(MAKE) -C player $(MYMAKEFLAGS) --no-print-directory all

pkg:
	@echo "===================================================="
	@echo "Building packaging... "
	@echo "  - Setting version $(VERSION) in debian files"
	@sed -ri "s/_VERSION_/$(VERSION)/g" debian/*
	@echo "  - Setting current date in changelog"
	@sed -ri "s/_DATE_/$(shell date -R)/g" debian/changelog

install:
	@echo "===================================================="
	@echo "Installing files... "
	@$(MAKE) -C player $(MYMAKEFLAGS) --no-print-directory install
	@$(MAKE) -C composer $(MYMAKEFLAGS) --no-print-directory install
