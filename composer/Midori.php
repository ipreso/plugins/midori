<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Media/Plugin/Skeleton.php';

class Media_Plugin_Midori extends Media_Plugin_Skeleton
{
    protected $_defaultDuration = '15';

    public function Media_Plugin_Midori ()
    {
        $this->_name        = 'Midori';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Web browser'));
    }

    public function isManagingFile ($path) { return (false); }
    public function addFile ($path) { return (false); }

    protected function _getFormattedLength ($inSeconds)
    {
        $hours  = intval ($inSeconds / 3600);
        $min    = intval (($inSeconds - ($hours * 3600)) / 60);
        $sec    = ($inSeconds - ($hours * 3600) - ($min * 60));

        if ($hours < 10)
            $hours = "0".$hours;
        if ($min < 10)
            $min = "0".$min;
        if ($sec < 10)
            $sec = "0".$sec;

        return ("$hours:$min:$sec");
    }

    public function getItems ()
    {
        $result = array ();

        $result ['midori'] =
            array (
                'name'      => $this->getTranslation ('Web'),
                'length'    => $this->_getFormattedLength (
                                            $this->_defaultDuration)
                  );

        return ($result);
    }

    public function getDefaultProperties ()
    {
        $properties = array ();

        // Web properties are :
        // - duration
        // - url

        $defaultWebProperties = new Media_Property ();
        $defaultWebProperties->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $this->_defaultDuration,
                                        'Seconds (0 = no end)'),
                    $this->createTextProperty (
                                        'url',
                                        $this->getTranslation ('URL'), 
                                        'http://www.google.fr',
                                        'http://some/site'),
                    $this->createSelectProperty (
                                        'cache',
                                        $this->getTranslation ('Preload'),
                                        array ('disabled'   => $this->getTranslation ('Disable'),
                                               'enabled'    => $this->getTranslation ('Enabled'),
                                               'enablednkey'=> $this->getTranslation ('Enabled and press L')),
                                        'disabled')));

        return ($defaultWebProperties);
    }

    public function addProperties ($propObj, $propArray)
    {
        foreach ($propArray as $key => $value)
        {
            $property = array ();
            switch ($key)
            {
                case "duration":
                    $property = $this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $value,
                                        'Seconds (0 = no end)');
                    break;
                case "url":
                    $property = $this->createTextProperty (
                                        'url', 
                                        $this->getTranslation ('URL'),
                                        $value,
                                        'http://some/site');
                    break;
                case "cache":
                    $property = $this->createSelectProperty (
                                        'cache',
                                        $this->getTranslation ('Preload'),
                                        array ('disabled'   => $this->getTranslation ('Disable'),
                                               'enabled'    => $this->getTranslation ('Enabled'),
                                               'enablednkey'=> $this->getTranslation ('Enabled and press L')),
                                        $value);
                    break;
            }
            $propObj->addProperty ($property);
        }

        return ($propObj);
    }

    public function serializeProperties ($item)
    {
        $IDs = $item->getSequenceProperties ();
        $properties = $item->getProperties ()->getValues ();

        // Item identifier
        $line = $this->_name."://".$IDs ['id']."/".$IDs ['name'].":";

        // Options
        foreach ($properties as $property)
        {
            switch ($property['attributes']['name'])
            {
                case "duration":
                    $line .= " duration=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "url":
                    $line .= " url=\""
                                .$this->getTextValue ($property)
                                ."\"";
                    break;
                case "cache":
                    $line .= " cache=\""
                                .$this->getSelectValue ($property)
                                ."\"";
                    break;
                default:
                    $line .= " # Don't know: ".$property ['attributes']['name'];
            }
        }
        return ($line);
    }

    public function unserializeProperties ($line)
    {
        // Get the hash
        $pattern = '/^([^:]+):\/\/([^\/]+)\/([^:]+): (.*)$/';
        if (preg_match ($pattern, $line, $matches))
            $hash = $matches [2];
        else
            $hash = 'UNKNOWN';

        // Parse options to return associated properties
        // - duration
        if (preg_match ('/.*duration="([0-9]+)".*/', $line, $matches))
            $duration = $matches [1];
        else
            $duration = $this->_defaultDuration;

        // - url
        if (preg_match ('/.*url="([^"]+)".*/', $line, $matches))
            $url = $matches [1];
        else
            $url = "http://www.google.fr";

        // - cache
        if (preg_match ('/.*cache="([a-z]+)".*/', $line, $matches))
            $cache = $matches [1];
        else
            $cache = 'disabled';
        if (strcmp ($cache, 'disabled') != 0 &&
            strcmp ($cache, 'enabled') != 0 &&
            strcmp ($cache, 'enablednkey') != 0)
            $cache = 'disabled';

        $prop = new Media_Property ();
        $prop->setItem ($hash);
        $prop->setValues (
            array ($this->createTextProperty (
                                        'duration', 
                                        $this->getTranslation ('Duration'), 
                                        $duration,
                                        'Seconds (0 = no end)'),
                    $this->createTextProperty (
                                        'url', 
                                        $this->getTranslation ('URL'),
                                        $url,
                                        'http://some/site'),
                    $this->createSelectProperty (
                                        'cache',
                                        $this->getTranslation ('Preload'),
                                        array ('disabled'   => $this->getTranslation ('Disable'),
                                               'enabled'    => $this->getTranslation ('Enabled'),
                                               'enablednkey'=> $this->getTranslation ('Enabled and press L')),
                                        $cache)));

        return ($prop);
    }

    public function getProperty ($propObj, $field)
    {
        $properties = $propObj->getValues ();
        foreach ($properties as $property)
        {
            if (strcmp ($property['attributes']['name'], $field) != 0)
                continue;

            switch ($field)
            {
                case "duration":
                    return ($this->getTextValue ($property));
                case "url":
                    return ($this->getTextValue ($property));
                case "cache":
                    return ($this->getSelectValue ($property));
            }
        }
        return (NULL);
    }
}
